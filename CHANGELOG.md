# v1.0.2
## 20/01/2020

**Improved:**

* Removed unnecessary descriptor from tutorial

# v1.0.1
## 17/01/2020

**Improved:**

  * Changed disk cache backend

**Bugfixes:**

  * Fixed incorrect package dependencies

# v1.0.0
## 14/01/2020

**New:**

  * ChangeLog started...
